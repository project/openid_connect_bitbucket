<?php

/**
 * @file
 * Provides a Bitbucket OpenID Connect client plugin.
 */

$plugin = array(
  'title' => t('Bitbucket'),
  'class' => 'OpenIDConnectBitbucketClient',
);
