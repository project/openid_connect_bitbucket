# OpenID Connect Bitbucket

Bitbucket doesn't support OpenID Connect. Nevertheless, this module
mocks the OpenID Connect flow to provide a client plugin for Bitbucket.

This is not affiliated with nor supported by Bitbucket in any way.

# References

* OpenID Connect module
  https://www.drupal.org/project/openid_connect
* Bitbucket OAuth documentation
  https://confluence.atlassian.com/display/BITBUCKET/OAuth+on+Bitbucket
* Bitbucket OAuth2 documentation
  *not available yet*
